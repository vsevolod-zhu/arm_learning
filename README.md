
为了便于大家学习ARM架构，收集相关文档

## 文档名称说明
为了名称统一微调了ARM官方文档的名称。格式一般为名称+文档编号+版本。例如a-profile_architecture_reference_manual_DDI0487H_a.pdf的名称是a-profile_architecture_reference_manual，文档编号是DDI0487，具体版本是H.a，文档编号和文档版本可以用于去ARM官网搜索更新版本。

## CPU
CPU的Technical Reference Manual（简称TRM），描述了该CPU的整体特性，编程模型，各个子系统的特性和寄存器，调测和外部信号等内容。

ARM开发的入门，推荐从TRM了解ARM系统。

CPU/cortex_a710_core_trm_101800_0201_07_en.pdf: 用于移动场景的ARMv9.0a处理器；
CPU/cortex_a72_mpcore_trm_100095_0003_06_en.pdf：树莓派4b的CPU；
CPU/neoverse_v1_trm_101427_0101_05_en.pdf：AWS graviton3的CPU。

## architecture
ARM架构相关文档，学习ARM架构的其中一个目标是能读懂更多架构相关的文档。

architecture/a-profile_architecture_reference_manual_DDI0487H_a.pdf：ARMv8, v9的架构手册，包括64位和32位两部分。

## peripheral
除CPU之外的IP，SMMU等子系统的文档。

peripheral/System_Memory_Management_Unit_Arm_Architecture_Specification_IHI_0070_D_a.pdf：ARM SMMU规范。

